Source: python-uritools
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Stein Magnus Jodal <jodal@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-pytest,
               python3-setuptools,
               python3-sphinx,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://github.com/tkem/uritools
Vcs-Git: https://salsa.debian.org/python-team/packages/python-uritools.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-uritools

Package: python3-uritools
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         ${sphinxdoc:Depends},
Built-Using: ${sphinxdoc:Built-Using},
Description: RFC 3986 compliant replacement for urlparse
 This module provides RFC 3986 compliant functions for parsing, classifying and
 composing URIs and URI references, largely replacing the Python Standard
 Library's urllib.parse module.
 .
 For various reasons, the Python 2 urlparse module is not compliant with
 current Internet standards, does not include Unicode support, and is generally
 unusable with proprietary URI schemes. Python 3's urllib.parse improves on
 Unicode support, but the other issues still remain.
 .
 This module aims to provide fully RFC 3986 compliant replacements for some
 commonly used functions found in urlparse and urllib.parse, plus additional
 functions for conveniently composing URIs from their individual components.
